package com.example.radesh.primeiraapp;

import android.app.Activity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import static android.media.MediaPlayer.create;


public class MainActivity extends Activity{

    ImageButton cachorro;
    MediaPlayer player = new MediaPlayer();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        player = create(this, R.raw.cachorro_completo);

        cachorro = (ImageButton) findViewById(R.id.imgCachorro);

        cachorro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.start();
                Toast.makeText(getApplicationContext(), ":(", Toast.LENGTH_LONG).show();
            }
        });

        }
    }

